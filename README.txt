Describe how and what each group member contributed for the past two weeks. If you are solo, these points will be part of the next section.

Hanzhi:

I aided the following ways 1) I joined the repository and tested if our git push/pull functionalities all worked and also successfully added the text file, verifying that the git system was working completely 2) I helped write the pseudocode for 
the doMove method with my partner on paper. 3) I implemented my code by myself, and my partner implemented his own code. My code had some bugs in the beginning as did his. 4) I helped revise our existent codes and helped draft the
 final doMove method 5) I helped with the testing of the method and adjusting  the heuristic values by running several tests against simple player. 6) I helped draft the pseudocode on paper for minimax 7) I implemented my own version of 
the code 8) I compared my version to my partner’s and revised/merged solutions to create the final product. 7) I aided in testing against the CPU several times.

 

Ashwin:

 

Ashwin: I aided in the following ways: 1) Setting up the team repository (I signed the team up and forked the assignment) 2) Writing the pseudocode (I helped write the psuedocode for the DoMove function). 3) I implemented the pseudocode 
and shared my code with my partner – it had some bugs to begin with 4) My partner shared his code that he implemented and we took the common elements and made the master code for the doMove method 5) I helped improve the functionality
 by testing against the simple player several times and adjusting the heuristic values – I helped with the trial and error process. 6) I helped write psuedocode for the minimax method on paper 7) I followed by implementing this code 8) I compared
 my code with my partner’s code and we merged our work to create the final minimax algorithm. 9) I helped in testing this program by running several tests against the CPU.



Document the improvements that your group made to your AI to make it tournament-worthy. Explain why you think your strategy(s) will work. Feel free to discuss any ideas were tried but didn't work out.

 

Improvement 1: Adding a heuristic {we later removed this because we used a minimax tree}: We set up a heuristic system attributing point values to different board indexes. For instance, we assigned the corners 4 points 
as they can’t be trapped by the opponent, -4 to the positions that are contiguous to the corners on the diagonal because they can easily be trapped by the opponent in 5 different ways, -2 to the positions next to the corners
 along the edges as they can be trapped in 3 different ways by the opponent, 2 points to the positions along the edges that are not contiguous to the corners as they are good for trapping the opponent, and 0 points to all of 
the rest of the positions because they don’t pose any particular value. We believe this strategy will work because it makes our player prioritize making moves on the side/edge/corners; this makes it harder for the opponent to trap us. 
Though we are not aggressive and tend to move along the edges, we strongly believe that the best offense is a good defense, which is manifested in this strategy.

 

Improvement 2: Creating a minimax tree: We set up a minimax tree that tests 3 layers of game routes the board can take and we picked the move that offers the least points at each move, assuming that our opponent is competent 
and will choose a move that maximizes their points. We believe this strategy works because we are probably going to be playing some very smart AIs out there, and playing the move that yields the most points at the surface level can
 give the opponent an easy opportunity to reverse fortunes on the next turn. Thus, by picking the least beneficial move at each step, we can minimize the gain of our opponent. Once again, our philosophy that a winning offense comes 
in good defense is prioritized in this strategy.