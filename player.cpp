#include "player.h"
//This is Hanzhi Lin
//This is Ashwin Hari (Change made to player1.cpp)

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
#include <stdlib.h>
#include <cmath>
#include <cstdio>
Player::Player(Side side) {
	// Will be set to true in test_minimax.cpp.
	testingMinimax = false;
	playerboard = new Board();
	s = side;
	/* 
	* TODO: Do any initialization you need to do here (setting up the board,
	* precalculating things, etc.) However, remember that you will only have
	* 30 seconds.
	*/
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	/* 
	* TODO: Implement how moves your AI should play here. You should first
	* process the opponent's opponents move before calculating your own move
	*/ 
	Side oppSide;

//The commented method below is for our Random Player:

/*	if (opponentsMove != NULL)
	{
		if (s == BLACK)
		{
                	oppSide = WHITE;
			playerboard->doMove(opponentsMove, oppSide);
       		}
		else
		{
			 oppSide = BLACK;
			playerboard->doMove(opponentsMove, oppSide);
		}
	}
	if (playerboard->hasMoves(s))
	{
		int x = rand()%8;
		int y = rand()%8;
		Move* playermove =  new Move(x, y);
		while(!(playerboard->checkMove(playermove, s)))
		{
			x = rand()%8;
			y = rand()%8;
			playermove =  new Move(x, y);
		}
		playerboard->doMove(playermove, s);
		return playermove;
	}
	else
	{
		return NULL;
	}*/

//The commented method below used only the Heuristic Method to beat Simple Player.
/*	if (opponentsMove != NULL)
	{
		if (s == BLACK)
		{
                	oppSide = WHITE;
			playerboard->doMove(opponentsMove, oppSide);
       		}
		else
		{
			 oppSide = BLACK;
			 playerboard->doMove(opponentsMove, oppSide);
		}
	}
	if (playerboard->hasMoves(s))
	{
		int max = -1000;
		int max_x;
		int max_y;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Board *copy = playerboard->copy();
				Move* copymove =  new Move(i, j);
				if(copy->checkMove(copymove, s))
				{
					copy->doMove(copymove, s);
					int score = copy->count(s) - copy->count(oppSide);
					if ((i, j) == (0, 0)) 
					{
						score += 3*abs(score);
					}
					else if ((i, j) == (7, 7))
					{
						score += 3*abs(score);
					}
					else if ((i, j) == (0, 7))
					{
						score += 3*abs(score);
					}
					else if ((i, j) == (7, 0))
					{
						score += 3*abs(score);
					}
					else if ((i, j) == (1, 0) or (i, j) == (0, 1) or (i, j) == (0, 6) or (i, j) == (1, 7))
					{
						score -= abs(score);
					}
				
					else if ((i, j) == (6, 0) or (i, j) == (7, 1) or (i, j) == (6, 7) or (i, j) == (7, 6))
					{
						score -= abs(score);
					}
					else if (i == 0)
					{
						score += abs(score);
					}
					else if (j == 7)
					{
						score += abs(score);
					}
					else if (i == 7)
					{
						score += abs(score);
					}
					else if (j == 0)
					{
						score += abs(score);
					}
					else if ((i, j) == (1, 1)) 
					{
						score -= 3*abs(score);
					}
					else if ((i, j) == (1, 6))
					{
						score -= 3*abs(score);
					}
					else if ((i, j) == (6, 1))
					{
						score -= 3*abs(score);
					}
					else if ((i, j) == (6, 6))
					{
						score -= 3*abs(score);
					}
					if (score > max)
					{
						max = score;
						max_x = i;	
						max_y = j;
					}
				}
				delete copy;
			}
		}
		Move* nextmove =  new Move(max_x, max_y);
		playerboard->doMove(nextmove, s);
		return nextmove;
	}
	else
	{
		return NULL;
	}*/

//The method below used the MinMax Method to beat Constant Time Player.

	if (opponentsMove != NULL)
	{
		if (s == BLACK)
		{
            oppSide = WHITE;
			playerboard->doMove(opponentsMove, oppSide);
       	}
		else
		{
			oppSide = BLACK;
			playerboard->doMove(opponentsMove, oppSide);
		}
	}
	if (playerboard->hasMoves(s))
	{
		int max = -1000;
		int max_x;
		int max_y;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Board* copy = playerboard->copy();
				Move* copymove =  new Move(i, j);
				if(copy->checkMove(copymove, s))
				{
					int min = 1000;
					copy->doMove(copymove, s);
					for (int a = 0; a < 8; a++)
					{
						for (int b = 0; b < 8; b++)
						{
							Board *copy2 = copy->copy();
							Move* copymove2 = new Move(a, b);
							if (copy2->checkMove(copymove2, oppSide))
							{
								copy2->doMove(copymove2, oppSide);
								int score = copy2->count(s) - copy2->count(oppSide);
								if (score < min)
								{
									min = score;
								}
							}
							delete copy2;
						}					
					}
					if (min > max && min != 1000)
					{
						max = min;
						max_x = i;
						max_y = j;
					}
				}
				delete copy;
			}
		}
		Move* nextmove =  new Move(max_x, max_y);
		playerboard->doMove(nextmove, s);
		return nextmove;
	}
	else
	{
		return NULL;
	}
}
